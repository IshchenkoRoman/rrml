import glob
import os

import numpy as np
from PIL import Image


class Solution(object):

    def __init__(self, target_folder=""):

        """
        Solution class makes for finding at least same images and sometimes
        similar. It based on comparing of histogram images.

        Work in progress: increase speed (CPython or Cython)

        Parameters:
            target_folder: str
                name of folder with data images

        Fields:
            self._target_folder: str
                name of folder

            self._images: dict
                dict:
                    keys- full path to image with name
                    values- difference between target histogram distribution and
                                compared image

            self._eps: float
                Some threshold barrier for finding similar images (maybe works
                not so good, if 2 images are blurred- so it may detected them,
                as similar images).
        """

        self._target_folder = target_folder
        self._images = None
        self._eps = 9.999e-05

    # -------------------------------- Getters ---------------------------------

    def get_target_image(self):

        return self._target_image

    def get_target_folder(self):

        return self._target_folder

    # -------------------------------- Setters ---------------------------------

    def set_target_image(self, target_image: str):

        assert isinstance(target_image, str)

        self._target_image = target_image

    def set_target_folder(self, target_folder: str):

        assert type(target_folder) == str

        self._target_folder = target_folder

    def _read_images(self, path_to_folder):

        """
        Read images from folder and store in self._images

        Parameters:
            path_to_folder: str
                path to folder, where live ponies and images

        Returns:
            data: dict
                dict:
                    keys- full path to image with name
                    values- difference between target histogram distribution and
                                compared image
        """

        formats = ("jpg", "jpeg", "png")

        # just works
        try:
            assert path_to_folder != None
            assert os.path.isdir(self._target_folder)

        except:
            print("Invalid direrctory argument")
            raise

        data = {}
        print(os.path.join(os.getcwd(), self._target_folder + "/*"))

        for f in glob.iglob(os.path.join(os.getcwd(), self._target_folder + "/*")):

            for end in formats:
                if f.endswith(end):
                    data[f] = Image.open(f)

        return data

    def _store_images(self, path_to_folder):

        """
        Save images, that were readed and store in self._images

        Parameters:
            path_to_folder: str
                path to folder, where live ponies and images

        Returns:
            None
        """

        self._images = self._read_images(path_to_folder)

        if not len(self._images):

            raise ValueError(f"Tricky: no images to compare in folder {path_to_folder}")

    def rgb2gray(self, rgb):

        """

        Apply grayscale filter to image

        Parameters:
            rgb: np.ndarray
                3 channels of image

        Returns:
            out: np.array
                1 channel rgb image.
        """

        return np.dot(rgb[..., :3], [0.2989, 0.5870, 0.1140])

    def solve(self):

        """
        Core logic of project, we take histogram distribution of target image
            and comapred image, find difference between this distributions, then
            we normalize, to be able comopare with some treshhold (self._eps).
            self._eps was taken by comparing some images based on data, that was
            given.

        Littly how I tried work with blured images(failed in reason that it
            consumes too much time). First of all I tried 'Sobel' filter to
            increase sharpness of images, minimum filter size was taken 7x7 and
            that was enoght, but results wasn't so good. After that I tried
            'skimage.restoration.wiener' but main problem- that I didn't know
            with what radius was blur applied and what kind of blur
            (for example: motion blur). Next I tried weird dunction of opencv
            I cann't  remember how they named (they have long name), but in
            examples they tested on girl in a spacesuit. So first fagged all RAM
            and second didn't give any results :_(
            Also feature detector (sift) with FlannBasedMatcher have pretty nice
            results, but opencv forbidden :_(
            And this 'https://github.com/KupynOrest/DeblurGAN' wasn't work :(

            So, task interesting, but I cann't spend more time on this- I'd have
            vacation, and maybe if you read this at moment- I spend it with my
            family) At last, I wish you a nice day and if you guessing around
            about some rest- take vacation :)

            https://www.youtube.com/watch?v=47dtFZ8CFo8
            https://www.youtube.com/watch?v=x-R19oA1m8w

        Parameters:
            None

        Returns:
            answer: dict
                dict:
                    keys- full path to image with name
                    values- difference between target histogram distribution and
                                compared image
        """

        if not self._images:
            self._store_images(self._target_folder)

        answer = {}

        for key, value in self._images.items():

            target_image_shape = np.array(self._images[key]).shape[:2]

            target_image_name = key
            results = {}

            for key_1, value_1 in self._images.items():

                current_image_shape = np.array(value_1).shape[:2]

                crops_shape = [i if i < j else j for i, j in
                               zip(target_image_shape, current_image_shape)]

                img_1 = self._images[target_image_name].crop((0, 0, *crops_shape))
                img_2 = value_1.crop((0, 0, *crops_shape))

                hist_1 = np.asarray(img_1.histogram())
                hist_2 = np.asarray(img_2.histogram())

                img_1 = img_1 / np.sum(img_1)
                img_2 = img_2 / np.sum(img_2)

                results[key_1] = np.absolute(np.linalg.norm(img_1 - img_2))

            answer[key.split("/")[-1]] = np.array(sorted([(v, k) for (k, v)
                                                          in results.items()]))

        return answer

    def print_results(self, answer):
        """

        Print final results if form target image: all intersections

        Parameters:
            answer: dict
                dict:
                    keys- full path to image with name
                    values- difference between target histogram distribution and
                                compared image

        Returns:
            None
        """

        string_blueprint = "{0} {1}"

        for batch_name, batch_values in answer.items():

            # unpack val (measure of similarity) and full path with name to image,
            # check if  it under treshhold (self._eps) and add only name to list
            treshold_images = [name_image.split("/")[-1] for val, name_image
                               in batch_values if float(val) < self._eps]

            print(string_blueprint.format(f"{batch_name}", " ".join(treshold_images)))

def main():

    sol = Solution("dev_dataset")
    answer = sol.solve()
    sol.print_results(answer)


if __name__ == "__main__":
    main()
