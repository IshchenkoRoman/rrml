The task is to develop a console tool which finds similar images in a given folder and prints similar pairs. We provide you with an example [dataset](https://drive.google.com/drive/folders/1A0aoCDbTK_XP61C-0H1i5F3Vu93KIHwM) for development. There are three types of similarity:
  - duplicate (images which are exactly the same) [+]
  - modification (images which differ by size, blur level and/or noise filters) [+/-]
  - similar (images of the same scene from another angle) [-]
 
The images are marked in the dataset with words in the file names that correspond to the type of similarity. The minimal acceptable solution should be able to find “duplicates”. The complete solution should handle all three types of similarity.


```
python3 main.py --path dev_dataset

11_duplicate.jpg 11.jpg 11_duplicate.jpg 11_c.jpg 11_modification.jpg un.jpg
11_c.jpg 11_c.jpg 11_modification.jpg un.jpg 11.jpg 11_duplicate.jpg
2.jpg 2.jpg
16.jpg 16.jpg
15_modification.jpg 15_modification.jpg
6.jpg 6.jpg
13.jpg 13.jpg
12.jpg 12.jpg
6_similar.jpg 6_similar.jpg
10.jpg 10.jpg
4_similar.jpg 4_similar.jpg
11.jpg 11.jpg 11_duplicate.jpg 11_c.jpg 11_modification.jpg un.jpg
3.jpg 3.jpg
1.jpg 1.jpg 1_duplicate.jpg
un.jpg un.jpg 11_c.jpg 11_modification.jpg 11.jpg 11_duplicate.jpg
11_modification.jpg 11_c.jpg 11_modification.jpg un.jpg 11.jpg 11_duplicate.jpg
4.jpg 4.jpg
1_duplicate.jpg 1.jpg 1_duplicate.jpg
14.jpg 14.jpg
8.jpg 8.jpg
15.jpg 15.jpg
```