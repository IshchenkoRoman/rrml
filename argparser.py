import argparse


def get_args():

    """
        Get parser arguments
    """

    parser = argparse.ArgumentParser(description="First test task on images\
                        similarity for Ruby on Rails Machine Learning school.")

    parser.add_argument("--path", "-p", help="Path to images", type=str)
    args = parser.parse_args()

    return args
