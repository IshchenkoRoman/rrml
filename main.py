from argparser import get_args
from solution import Solution


def main():

    """
        Enterpoint for launch code
    """

    args = get_args()
    print(args)

    sol = Solution(args.path)
    answer = sol.solve()
    sol.print_results(answer)

if __name__ == "__main__":
    main()
